var gulp = require('gulp'),
    concat = require('gulp-concat'),
    rename = require("gulp-rename"),
    uglify = require('gulp-uglify'),
    git = require('gulp-git'),
    codePaths = ['app.js'];



module.exports = {
    default: gulp.series(dist, watch),
    dist,
    watch
};

function dist() {
    return gulp.src(codePaths)
        .pipe(concat('sp-vertical-value-slider.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add())
        .pipe(uglify({
            compress: true
        }))
        .pipe(rename('sp-vertical-value-slider.min.js'))
        .pipe(gulp.dest('dist'))
        .pipe(git.add());
}

function watch() {
    gulp.watch(codePaths, dist);
}
