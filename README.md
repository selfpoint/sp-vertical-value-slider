#sp-vertical-value-slider
animation on input type number directive for angularjs

##options
* `sp-model` - like ng-model - required
* `sp-change` - working only if tag-name is input and call the function when changing only from the directive inputs  - optional
* `sp-tag-name` - element tag-name (input, span) - default: `input`
* `sp-disabled` - element like ng-disabled - default: `false`

##example
```
<sp-vertical-value-slider sp-model="test" sp-change="alert('a');"></sp-vertical-value-slider>
```

##commit
before any commit do `gulp` (to create dist file)